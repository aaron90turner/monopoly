package edu.ncsu.monopoly;

public abstract class Cell implements IOwnable  {
	private String name;
	/* (non-Javadoc)
	 * @see edu.ncsu.monopoly.IOwnable1#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	public int getPrice() {
		return 0;
	}

	
	
	/* (non-Javadoc)
	 * @see edu.ncsu.monopoly.IOwnable#playAction(java.lang.String)
	 */

	/* (non-Javadoc)
	 * @see edu.ncsu.monopoly.IOwnable1#playAction(java.lang.String)
	 */
	
	@Override
	public abstract boolean playAction(String msg);

	void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}
}
