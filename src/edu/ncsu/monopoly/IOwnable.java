package edu.ncsu.monopoly;

public interface IOwnable {
	
	public abstract String getName();

	
	public abstract Player getTheOwner();

	public abstract boolean playAction(String msg);

}