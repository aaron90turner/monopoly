package edu.ncsu.monopoly;

public abstract class OwnedCell extends Cell {

	private boolean available = true;
	protected Player theOwner;

	public OwnedCell() {
		super();
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	@Override
	public Player getTheOwner() {
		return theOwner;
	}

	public void setTheOwner(Player owner) {
		this.theOwner = owner;
	}

}